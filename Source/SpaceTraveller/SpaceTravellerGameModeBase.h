// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SpaceTravellerGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SPACETRAVELLER_API ASpaceTravellerGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
