// Copyright Epic Games, Inc. All Rights Reserved.

#include "SpaceTraveller.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, SpaceTraveller, "SpaceTraveller" );
