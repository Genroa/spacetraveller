Event	ID	Name			Wwise Object Path	Notes
	790938957	button_unpush			\Default Work Unit\button\button_unpush	
	1093986015	Play_drone__interior_spaceship			\Default Work Unit\Play_drone__interior_spaceship	
	1701177942	button_hover			\Default Work Unit\button\button_hover	
	1965951612	button_push			\Default Work Unit\button\button_push	
	2623731151	Play_drone_test			\Default Work Unit\Play_drone_test	
	2801770543	Play_audio_test			\Default Work Unit\Play_audio_test	

Switch Group	ID	Name			Wwise Object Path	Notes
	753474920	button_type			\Default Work Unit\button_type	

Switch	ID	Name	Switch Group			Notes
	2455443530	leboutoncarre	button_type			
	2505162442	UI_button	button_type			

Game Parameter	ID	Name			Wwise Object Path	Notes
	1240670792	distance			\Default Work Unit\distance	
	3715552714	nge_volume			\Default Work Unit\nge_volume	
	4216092624	drone_test			\Default Work Unit\drone_test	

In Memory Audio	ID	Name	Audio source file		Wwise Object Path	Notes	Data Size
	66256317	pidibidipoudoudadou	D:\SOUND DESIGN\PRO\spacetraveller\Wwise_project\Space_Traveler_wwiseproj\.cache\Windows\SFX\pidibidipoudoudadou_10C4C929.wem		\Actor-Mixer Hierarchy\Default Work Unit\trolling_to_delete\pidibidipoudoudadou		6784392
	90344330	music	D:\SOUND DESIGN\PRO\spacetraveller\Wwise_project\Space_Traveler_wwiseproj\.cache\Windows\SFX\music_for_genroa_D6213305.wem		\Actor-Mixer Hierarchy\Default Work Unit\trolling_to_delete\music		17350904
	119703596	drone__interior_spaceship_02	D:\SOUND DESIGN\PRO\spacetraveller\Wwise_project\Space_Traveler_wwiseproj\.cache\Windows\SFX\drone__interior_spaceship-glued_C669900E.wem		\Actor-Mixer Hierarchy\Default Work Unit\spaceship\drone_space\drone__interior_spaceship_02		5331800
	291323419	drone__interior_spaceship_03	D:\SOUND DESIGN\PRO\spacetraveller\Wwise_project\Space_Traveler_wwiseproj\.cache\Windows\SFX\drone__interior_spaceship-glued_922300AA.wem		\Actor-Mixer Hierarchy\Default Work Unit\spaceship\drone_space\drone__interior_spaceship_03		6137316
	379865733	drone__interior_spaceship_04	D:\SOUND DESIGN\PRO\spacetraveller\Wwise_project\Space_Traveler_wwiseproj\.cache\Windows\SFX\drone__interior_spaceship-glued_598F3BDA.wem		\Actor-Mixer Hierarchy\Default Work Unit\spaceship\drone_space\drone__interior_spaceship_04		6520892
	688294184	hover	D:\SOUND DESIGN\PRO\spacetraveller\Wwise_project\Space_Traveler_wwiseproj\.cache\Windows\SFX\hover_E35550D5.wem		\Actor-Mixer Hierarchy\Default Work Unit\spaceship\button\button_hover_type\hover		15460
	725036532	drone__interior_spaceship_01	D:\SOUND DESIGN\PRO\spacetraveller\Wwise_project\Space_Traveler_wwiseproj\.cache\Windows\SFX\drone__interior_spaceship-glued_6BE1122C.wem		\Actor-Mixer Hierarchy\Default Work Unit\spaceship\drone_space\drone__interior_spaceship_01		3644052
	735630850	drone__interior_spaceship	D:\SOUND DESIGN\PRO\spacetraveller\Wwise_project\Space_Traveler_wwiseproj\.cache\Windows\SFX\drone__interior_spaceship-glued_ACB7EC74.wem		\Actor-Mixer Hierarchy\Default Work Unit\spaceship\drone_space\drone__interior_spaceship		3835844
	965739239	button_unpush	D:\SOUND DESIGN\PRO\spacetraveller\Wwise_project\Space_Traveler_wwiseproj\.cache\Windows\SFX\button_push_4FC8420A.wem		\Actor-Mixer Hierarchy\Default Work Unit\spaceship\button\button_unpush_type\button_unpush		18892
	988526855	button_push	D:\SOUND DESIGN\PRO\spacetraveller\Wwise_project\Space_Traveler_wwiseproj\.cache\Windows\SFX\button_push_283A7445.wem		\Actor-Mixer Hierarchy\Default Work Unit\spaceship\button\button_push_type\button_push		97564
	1021309388	push_UI	D:\SOUND DESIGN\PRO\spacetraveller\Wwise_project\Space_Traveler_wwiseproj\.cache\Windows\SFX\push_UI_F41797D8.wem		\Actor-Mixer Hierarchy\Default Work Unit\spaceship\button\button_push_type\push_UI		107012

