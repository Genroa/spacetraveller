#pragma once

#ifdef AK_GX

#include "Platforms/AkPlatformBase.h"
#include "AkXboxOneGDKInitializationSettings.h"

#define TCHAR_TO_AK(Text) (const WIDECHAR*)(Text)

using UAkInitializationSettings = UAkXboxOneGDKInitializationSettings;

struct FAkGXPlatform : FAkPlatformBase
{
	static const UAkInitializationSettings* GetInitializationSettings()
	{
		return GetDefault<UAkXboxOneGDKInitializationSettings>();
	}

	static const FString GetPlatformBasePath()
	{
		return FString("XboxOneGDK");
	}
};

using FAkPlatform = FAkGXPlatform;

#endif
